extern crate android_sparse as sparse;

mod util;

use self::util::{data_file, test_blocks};
use sparse::{Block, Encoder, Reader};

#[test]
fn read_sparse() {
    let file = data_file("hello.simg");
    let expected = test_blocks();

    let reader = Reader::new(file).unwrap();
    let blocks: Vec<_> = reader.map(std::result::Result::unwrap).collect();
    assert_eq!(blocks.len(), expected.len());

    for (blk, exp) in blocks.iter().zip(expected.iter()) {
        assert_eq!(blk, exp);
    }
}

#[test]
fn read_sparse_with_crc() {
    let file = data_file("crc.simg");
    let mut expected = test_blocks();
    expected.push(Block::Crc32(0xffb8_80a5));

    let reader = Reader::with_crc(file).unwrap();
    let blocks: Vec<_> = reader.map(std::result::Result::unwrap).collect();
    assert_eq!(blocks.len(), expected.len());

    for (blk, exp) in blocks.iter().zip(expected.iter()) {
        assert_eq!(blk, exp);
    }
}

#[test]
fn read_sparse_with_invalid_crc() {
    let file = data_file("invalid_crc.simg");

    let mut reader = Reader::with_crc(file).unwrap();
    assert!(reader.nth(5).unwrap().is_err());
}

#[test]
fn encode_raw() {
    let file = data_file("hello.img");
    let expected = test_blocks();

    let encoder = Encoder::new(file).unwrap();
    let blocks: Vec<_> = encoder.map(std::result::Result::unwrap).collect();
    assert_eq!(blocks.len(), expected.len());

    for (blk, exp) in blocks.iter().zip(expected.iter()) {
        assert_eq!(blk, exp);
    }
}
